<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  header("location: login.php");
  exit;
}

include_once "connection.php";



$sql = "select * from upcoming_project";
$list_projects = $link->query($sql);

require "header.php";
?>

<div class="pagetitle">
  <h1>UPCOMING PROJECTS</h1>
  <br>
  <hr><br>
</div><!-- End Page Title -->
<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <a type="button" class="btn btn-primary float-right m-2" href="add-project.php"><b>+</b> Add Upcoming Project</a>

      <br>
      <div style="float: right;">
        <span>Filter : </span>
        <div class="form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optradio" value="ido" checked> IDO
          </label>
        </div>
        <div class="form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optradio" value="listed"> Listed
          </label>
        </div>
        <div class="form-check-inline disabled">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optradio" value="launched"> Launched
          </label>
        </div>
      </div>


      <table class="table list-table">
        <thead>
          <tr>
            <th>Project name</th>
            <th>Rating</th>
            <th id="th-dates" data-type="@data-sort">Dates</th>
            <th>IDO price</th>
            <th>Supply</th>
            <th>Market cap</th>
            <th>Links</th>
            <th>Action</th>

          </tr>
          <thead>
          <tbody>
            <?php
            if ($list_projects->num_rows > 0) {
              // output data of each row
              while ($row = $list_projects->fetch_assoc()) {
            ?>
                <tr>
                  <td>
                    <b>
                      <h3><?php echo $row['project_name']; ?> </h3>
                    </b>

                  </td>
                  <td>
                    <b>
                      <h3><?php echo $row['rating']; ?> </h3>
                    </b>
                  </td>
                  <td class="dates" data-sort="<?php $mydate = strtotime($row['ido_date']);
                                                echo $mydate; ?>" data-ido="<?php $mydate = strtotime($row['ido_date']);
                                                                            echo $mydate; ?>" data-listing="<?php $mydate = strtotime($row['listing_date']);
                                                                                                            echo $mydate; ?>" data-p2e="<?php $mydate = strtotime($row['p2e_launch']);
                                                                                                                                        echo $mydate; ?> ">
                    <b>
                      <p>IDO : <?php echo $row['ido_date']; ?> </p>
                      <p>Listing : <?php echo $row['listing_date']; ?> </p>
                      <p>P2E : <?php echo $row['p2e_launch']; ?> </p>
                    </b>

                  </td>
                  <td>
                    <b>
                      <?php echo $row['ido_price']; ?>
                    </b>
                  </td>
                  <td>
                    <b>
                      <p>Total : <?php echo $row['initial_supply']; ?> </p>
                      <p>Initial : <?php echo $row['total_supply']; ?> </p>
                    </b>


                  </td>
                  <td>
                    <b>
                      <p>Initial : <?php echo $row['initial_market']; ?> </p>
                    </b>
                  </td>
                  <td>
                    <a href="<?php echo $row['link']; ?>"><i class="bi bi-globe2" style="font-size: 42px;"></i></a>

                  </td>
                  <td>
                    <button data-id="<?php echo $row['id']; ?>" class="btn btn-sm btn-danger delete-project" style="margin-right: 4px;" data-toggle="tooltip" data-placement="top" title="Delete">
                      <i class="bi bi-trash" style="-webkit-text-fill-color: unset;"></i>
                    </button>
                  </td>

                </tr>
            <?php

              }
            }

            ?>
          </tbody>
          <table />


          <!-- <iframe id="inlineFrameExample" title="Inline Frame Example" width="1000" height="600" src="./table-iframe.php"> -->
          </iframe>

          <!-- delete Modal -->

          <div class="modal fade" id="deleteModal" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Delete project</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                  <h4>Do you want to delete this project ?</h4>
                </div>
                <div class="modal-footer">
                  <a href="#" class="btn  btn-danger " id="delete-project" style="margin-right: 4px;" data-toggle="tooltip" data-placement="top" title="Delete">
                    Delete
                  </a>
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>

                </div>
              </div>
            </div>
          </div><!-- End Basic Modal-->

    </div>
  </div>
</section>


<?php
require "footer.php";
?>

<script>
  $(document).on('click', '.delete-project', function() {
    var id_delete = $(this).data('id');
    $('#delete-project').attr('href', "delete-project.php?id=" + id_delete);
    $('#deleteModal').modal('show');

  })

  $('input[name="optradio"]').on('click', function() {
    filter_date = $('input[name="optradio"]:checked').val();
    td_dates = $('.dates')

    if (filter_date == "ido") {
      td_dates.each(function() {
        $(this).attr('data-sort', $(this).attr('data-ido'))
      })
    }
    if (filter_date == "listed") {
      td_dates.each(function() {
        $(this).attr('data-sort', $(this).attr('data-listing'))
      })
    }
    if (filter_date == "launched") {
      td_dates.each(function() {
        $(this).attr('data-sort', $(this).attr('data-p2e'))
      })
    }

    $(".list-table").DataTable().rows().invalidate();
    $('#th-dates').click();
    $('#th-dates').click();


  });
</script>