<?php

include_once "connection.php";

$sql = "select * from upcoming_project";
$list_projects = $link->query($sql);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Token Flippers</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <!-- <link href="assets/img/favicon.png" rel="icon"> -->
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet">
    <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet">

    <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/table.css" rel="stylesheet">


    <!-- =======================================================
  * Template Name: NiceAdmin - v2.1.0
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
    <style>
        th {
            font-size: 13px;
        }

        td {
            font-size: 12px;
        }
    </style>
</head>

<body>





    <main id="main" class="main" style="margin: unset;">

        <div class="pagetitle">
            <h1>UPCOMING PROJECTS</h1>
            <br>
            <hr><br>
        </div><!-- End Page Title -->
        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div style="float: right;">
                        <span>Filter : </span>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optradio" value="ido" checked> IDO
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optradio" value="listed"> Listed
                            </label>
                        </div>
                        <div class="form-check-inline disabled">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optradio" value="launched"> Launched
                            </label>
                        </div>
                    </div>
                    <table class="table list-table">
                        <thead>
                            <tr>
                                <th>Project name</th>
                                <th>Rating</th>
                                <th id="th-dates" data-type="@data-sort">Dates</th>
                                <th>IDO price</th>
                                <th>Supply</th>
                                <th>Market cap</th>
                                <th>Links</th>

                            </tr>
                            <thead>
                            <tbody>
                                <?php
                                if ($list_projects->num_rows > 0) {
                                    // output data of each row
                                    while ($row = $list_projects->fetch_assoc()) {
                                ?>
                                        <tr>
                                            <td>
                                                <b>
                                                    <h3><?php echo $row['project_name']; ?> </h3>
                                                </b>

                                            </td>
                                            <td>
                                                <b>
                                                    <h3><?php echo $row['rating']; ?> </h3>
                                                </b>
                                            </td>
                                            <td class="dates" data-sort="<?php $mydate = strtotime($row['ido_date']);
                                                                            echo $mydate; ?>" data-ido="<?php $mydate = strtotime($row['ido_date']);
                                                                                                        echo $mydate; ?>" data-listing="<?php $mydate = strtotime($row['listing_date']);
                                                                                                            echo $mydate; ?>" data-p2e="<?php $mydate = strtotime($row['p2e_launch']);
                                                                                                                                        echo $mydate; ?> ">
                                                <b>
                                                    <p>IDO : <?php echo $row['ido_date']; ?> </p>
                                                    <p>Listing : <?php echo $row['listing_date']; ?> </p>
                                                    <p>P2E : <?php echo $row['p2e_launch']; ?> </p>
                                                </b>

                                            </td>
                                            <td>
                                                <b>
                                                    <?php echo $row['ido_price']; ?>
                                                </b>
                                            </td>
                                            <td>
                                                <b>
                                                    <p>Total : <?php echo $row['initial_supply']; ?> </p>
                                                    <p>Initial : <?php echo $row['total_supply']; ?> </p>
                                                </b>


                                            </td>
                                            <td>
                                                <b>
                                                    <p>Initial : <?php echo $row['initial_market']; ?> </p>
                                                </b>
                                            </td>
                                            <td>
                                                <a href="<?php echo $row['link']; ?>"><i class="bi bi-globe2" style="font-size: 42px;"></i></a>

                                            </td>


                                        </tr>
                                <?php

                                    }
                                }

                                ?>
                            </tbody>
                            <table />




                </div>
            </div>
        </section>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->



    <!-- Vendor JS Files -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" integrity="sha256-/H4YS+7aYb9kJ5OKhFYPUjSJdrtV6AeyJOtTkw6X72o=" crossorigin="anonymous"></script>

    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="assets/vendor/quill/quill.min.js"></script>
    <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>


    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
    <script>
        $('input[name="optradio"]').on('click', function() {
            filter_date = $('input[name="optradio"]:checked').val();
            td_dates = $('.dates')

            if (filter_date == "ido") {
                td_dates.each(function() {
                    $(this).attr('data-sort', $(this).attr('data-ido'))
                })
            }
            if (filter_date == "listed") {
                td_dates.each(function() {
                    $(this).attr('data-sort', $(this).attr('data-listing'))
                })
            }
            if (filter_date == "launched") {
                td_dates.each(function() {
                    $(this).attr('data-sort', $(this).attr('data-p2e'))
                })
            }

            $(".list-table").DataTable().rows().invalidate();
            $('#th-dates').click();
            $('#th-dates').click();


        });
    </script>

</body>

</html>