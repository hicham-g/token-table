<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

include_once "connection.php";

require "header.php";
?>



<div class="card">
    <div class="card-body">
        <h5 class="card-title text-center">Add upcoming project</h5>

        <!-- Vertical Form -->
        <form class="row g-3" action="" method="post" enctype="multipart/form-data">
            <div class="col-6">
                <label for="project-name" class="form-label">Project name</label>
                <input type="text" class="form-control" id="project-name" name="project-name" required>
            </div>
            <div class="col-6">
                <label for="rating" class="form-label">Rating</label>
                <input type="number" class="form-control" id="rating" name="rating" required>
            </div>
            <div class="col-4">
                <label for="ido-date" class="form-label">IDO date</label>
                <input type="date" class="form-control" id="ido-date" name="ido-date">
            </div>
            <div class="col-4">
                <label for="listing-date" class="form-label">Listing date</label>
                <input type="date" class="form-control" id="listing-date" name="listing-date">
            </div>
            <div class="col-4">
                <label for="p2e-launch" class="form-label">P2E launch</label>
                <input type="date" class="form-control" id="p2e-launch" name="p2e-launch">
            </div>
            <div class="col-6">
                <label for="ido-price" class="form-label">IDO price</label>
                <input type="text" class="form-control" id="ido-price" name="ido-price" required>
            </div>
            <div class="col-6">
                <label for="total-supply" class="form-label">Total supply</label>
                <input type="text" class="form-control" id="total-supply" name="total-supply" required>
            </div>
            <div class="col-6">
                <label for="initail-supply" class="form-label">Inital supply</label>
                <input type="text" class="form-control" id="initail-supply" name="initial-supply" required>
            </div>
            <div class="col-6">
                <label for="initial-market" class="form-label">Initail market cap</label>
                <input type="text" class="form-control" id="initial-market" name="initial-market" required>
            </div>
            <div class="col-12">
                <label for="link" class="form-label">link</label>
                <input type="text" class="form-control" id="link" name="link" required>
            </div>


    </div>
    <div class="text-center mb-5">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a type="reset" class="btn btn-secondary" href="users-index.php">Cancel</a>
    </div>
    </form><!-- Vertical Form -->

</div>
</div>

<?php
require "footer.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $project_name = $_POST['project-name'];
    $rating = $_POST['rating'];
    $ido_date = $_POST['ido-date'];
    $listing_date = $_POST['listing-date'];
    $ido_price = $_POST['ido-price'];
    $total_supply = $_POST['total-supply'];
    $initial_supply = $_POST['initial-supply'];
    $pe2_launch = $_POST['p2e-launch'];
    $initial_market = $_POST['initial-market'];
    $req_link = $_POST['link'];



    $sql = "INSERT INTO upcoming_project (project_name , rating , ido_date , listing_date , ido_price ,  total_supply , initial_supply , p2e_launch , initial_market , link )
     VALUES ('$project_name',$rating,'$ido_date' , '$listing_date','$ido_price' , '$total_supply','$initial_supply','$pe2_launch','$initial_market','$req_link')";
    if (mysqli_query($link, $sql)) {
?>

        <script type="text/javascript">
            window.location = "index.php";
        </script>
<?php

    } else {
        echo "Error: " . $sql . ":-" . mysqli_error($link);
    }
    mysqli_close($link);
}


?>

<script>

</script>